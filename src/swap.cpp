/*
 * @author	{\_Sidewinder22_/}
 * @date	15 maj 2021
 * 
 * @brief   Swap example
 */

#include <iostream>

struct Clock
{
    int tic;
};


struct Counter
{
    Clock clock;
};

void swap(Counter &l, Counter &r) {
    Clock tmp = l.clock;
    l.clock = r.clock;
    r.clock = tmp;
}

int main()
{
    Clock clock1;
    clock1.tic = 1;

    Clock clock2;
    clock2.tic = 2;

    Counter c1;
    c1.clock = clock1;

    Counter c2;
    c2.clock = clock2;

    std::cout << "Before: "
        << "c1 = " << c1.clock.tic
        << ", c2 = " << c2.clock.tic
        << std::endl;

    swap(c1, c2);

    std::cout << "After: "
        << "c1 = " << c1.clock.tic
        << ", c2 = " << c2.clock.tic
        << std::endl;

    return 0;
}
