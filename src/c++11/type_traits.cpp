/*
 * type_traits.cpp
 *
 *  Created on: 22 maj 2021
 *      Author: sidewin
 */

#include <iostream>
#include <type_traits>

struct Foo
{ };

int main()
{
	std::cout << std::boolalpha;

	std::cout << "Foo is object = " << std::is_object<Foo>::value << std::endl;
	std::cout << "Foo& is object = " << std::is_object<Foo&>::value << std::endl;
	std::cout << "Foo&& is object = " << std::is_object<Foo&&>::value << std::endl;

	std::cout << "Foo is lvalue = " << std::is_lvalue_reference<Foo>::value << std::endl;
	std::cout << "Foo& is lvalue = " << std::is_lvalue_reference<Foo&>::value << std::endl;
	std::cout << "Foo&& is lvalue = " << std::is_lvalue_reference<Foo&&>::value << std::endl;

	std::cout << "Foo is rvalue = " << std::is_rvalue_reference<Foo>::value << std::endl;
	std::cout << "Foo& is rvalue = " << std::is_rvalue_reference<Foo&>::value << std::endl;
	std::cout << "Foo&& is rvalue = " << std::is_rvalue_reference<Foo&&>::value << std::endl;

	return 0;
}
